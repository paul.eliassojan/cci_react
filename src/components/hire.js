import React, { Component } from "react";
import "../css/hire.css";
import Hire from "../images/hire1.png";

import { Row, Col, Container } from "react-bootstrap";

class App extends Component {
  render() {
    function updateTimer(deadline) {
      var time = deadline - new Date();
      return {
        days: Math.floor(time / (1000 * 60 * 60 * 24)),
        hours: Math.floor((time / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((time / 1000 / 60) % 60),
        seconds: Math.floor((time / 1000) % 60),
        total: time,
      };
    }

    function animateClock(span) {
      span.className = "animate__flipOutY";
      setTimeout(function () {
        span.className = "";
      }, 1000);
    }

    function startTimer(id, deadline) {
      var timerInterval = setInterval(function () {
        var clock = document.getElementById(id);
        var timer = updateTimer(deadline);

        clock.innerHTML =
          "<span>" +
          timer.days +
          "</span>" +
          "<span>" +
          timer.hours +
          "</span>" +
          "<span>" +
          timer.minutes +
          "</span>" +
          "<span>" +
          timer.seconds +
          "</span>";

        //animation
        var spans = clock.getElementsByTagName("span");
        animateClock(spans[3]);
        if (timer.seconds === 59) animateClock(spans[2]);
        if (timer.minutes === 59 && timer.seconds === 59)
          animateClock(spans[1]);
        if (timer.hours === 23 && timer.minutes === 59 && timer.seconds === 59)
          animateClock(spans[0]);

        //check for end of time

        if (timer.total < 1) {
          clearInterval(timerInterval);
          clock.innerHTML =
            "<span>0</span><span>0</span><span>0</span><span>0</span>";
        }
      }, 1000);
    }

    window.onload = function () {
      var deadline = new Date("July 17, 2020 12:00:00");
      startTimer("clock", deadline);
    };

    return (
      <>
        <div className="pa">
          <div className="home">
            <div className="d-none d-lg-block">
              <div class="animated-title">
                <div class="text-top">
                  <div>
                    <span>We Are</span>
                    <span></span>
                  </div>
                </div>
                <div class="text-bottom">
                  <div>Hiring !</div>
                </div>
              </div>
              <Row>
                <Col></Col>

                <Col>
                  <div className="col text-center">
                    <img
                      alt="Hiring"
                      src={Hire}
                      style={{ paddingTop: 100, width: "70%" }}
                      className="img-fluid mx-auto"
                    ></img>
                  </div>
                </Col>
              </Row>
            </div>

            <div className="d-block d-sm-block d-md-none">
              <div style={{ left: "50%", top: "12rem" }} class="animated-title">
                <div class="text-top">
                  <div>
                    <span>We Are</span>
                    <span></span>
                  </div>
                </div>
                <div class="text-bottom">
                  <div>Hiring!</div>
                </div>
              </div>
              <Row>
                <Col>
                  <div className="col text-center">
                    <img
                      alt="Hiring"
                      src={Hire}
                      style={{ paddingTop: 300 }}
                      className="img-fluid mx-auto"
                    ></img>
                  </div>
                </Col>
              </Row>
            </div>
          </div>

          <Container>
            <div class="page">
              <div class="page__section">
                <div class="page__group">
                  <h3 class="r-title news__title">
                    <a
                      style={{ fontWeight: "bold" }}
                      href="#0"
                      class="r-link animated-underline animated-underline_type2 news__link"
                    >
                      Want to be a part of something great...
                    </a>
                  </h3>
                </div>
              </div>
            </div>
          </Container>
          <div id="main">
            <div id="clock"></div>
            <div id="display">
              <span>Days</span> <span>Hours</span> <span>Minutes</span>
              <span>Seconds</span>
            </div>
          </div>
          <div className="d-none d-lg-block">
            <Container>
              <Row class="justify-content-center">
                <Col xs={6} style={{ paddingTop: 50, paddingBottom: 50 }}>
                  <a href="http://cci-exam.herokuapp.com/">
                    <button class="btn-blue">Apply</button>
                  </a>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="d-block d-sm-block d-md-none">
            <Container>
              <Row class="justify-content-center">
                <Col xs={6} style={{ paddingTop: 50, paddingBottom: 50 }}>
                  <a href="http://cci-exam.herokuapp.com/">
                    <button class="btn-blue1">Apply</button>
                  </a>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </>
    );
  }
}
export default App;
